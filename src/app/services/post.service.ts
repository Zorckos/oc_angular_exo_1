import { Subject } from "rxjs";

export class PostService {
  private posts = [
    {
      id: 1,  
      title : 'La guerre des viennoiserie',
      created_at : new Date(),
      content : 'Il y à très longtemps dans un temps lointain... Genre une semaine, une guerre se déclara entre le clan des Pains au chocolat et celui des Chocolatines.......',
      loveIts: 0
    },
    {
      id: 2,  
      title :  'Armistice entre les Pains au chocolat et les chocolatines',
      created_at :  new Date(),
      content :  'Suite à un long conflit de perte de viennoiseries, les deux chefs des clans en guerre decidérent de faire la paix.',
      loveIts: 0
    },
    {
      id: 3,  
      title : 'La guerre du Mont Saint-Michel',
      created_at :  new Date(),
      content :  'Encore une guerre de déclarée, cette fois çi le conflit à lieu entre les Normands et Bretons.',
      loveIts: 0
    }
  ];

  postSubject = new Subject<any[]>();

  emitPostSubject() {
    this.postSubject.next(this.posts.slice());
  }
}