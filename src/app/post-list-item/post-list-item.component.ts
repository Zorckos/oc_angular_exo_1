import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title : string = 'title';
  @Input() created_at : Date = new Date();
  @Input() content : string = 'tototo';
  @Input() loveIts: number;


  constructor() { }

  ngOnInit() {
    this.loveIts = 0;
  }

  onLoveIt(){
    this.loveIts++;
    console.log(this.loveIts);
  }

  onDontLoveIt(){
    this.loveIts--;
    console.log(this.loveIts);
  }

  getColor(){
    if(this.loveIts === 0){
      return 'black';
    } else if(this.loveIts > 0){
      return 'green';
    } else if(this.loveIts < 0){
      return 'red';
    }
  }
}
